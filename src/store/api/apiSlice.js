import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { BASE_URI, FETCH_MUTEX } from "@/features/constants";
import { cookieGet, cookieSet, localSettings } from "@/features/helpers";
// import { logout } from "@/features/slices/User/logout";

const baseQuery = fetchBaseQuery({
  baseUrl: BASE_URI,
  prepareHeaders: (headers) => {
    const token = cookieGet("at");
    if (token) {
      headers.set("authorization", `Bearer ${token}`);
    }
    return headers;
  },
  responseHandler: "content-type",
});

export const baseQueryWithReAuth = async (args, api, extraOptions) => {
  let result = await baseQuery(args, api, extraOptions);
  // let getRefreshToken;

  if (result.error && result.error.status === 429) {
    return { ...result, error: { status: 429, error: "Too Many Requests" } };
  }
  if (result.error && result.error.status === 401) {
    // getRefreshToken = () => cookieGet("rt");
    if (!FETCH_MUTEX.isLocked()) {
      const release = await FETCH_MUTEX.acquire();

      try {
        const refreshResult = await baseQuery(
          {
            // url: `/auth/refresh-token?refreshToken=${getRefreshToken()}`,
            url: `/auth/refresh-token?refreshToken`,
            method: "POST",
          },
          api,
          extraOptions
        );

        if (refreshResult.data) {
          const newTokensData = refreshResult.data.data;

          localSettings.set("token", newTokensData.token);
          localSettings.set("refresh_token", newTokensData.refresh_token);

          cookieSet("at", newTokensData.token);
          cookieSet("rt", newTokensData.refresh_token);

          const newTokenEvent = new CustomEvent("newTokenEvent", { detail: newTokensData.token });
          window.dispatchEvent(newTokenEvent);

          result = await baseQuery(args, api, extraOptions);
        } else {
          // await logout();
        }
      } catch (error) {
        // await logout();
      } finally {
        release();
      }
    } else {
      await FETCH_MUTEX.waitForUnlock();
      result = await baseQuery(args, api, extraOptions);
    }
  }

  return result;
};

export const apiSlice = createApi({
  baseQuery: baseQueryWithReAuth,
  endpoints: () => ({}),
});

export const { usePrefetch } = apiSlice;
