import { createSlice } from "@reduxjs/toolkit";

export const _modalSlice = createSlice({
  name: "modal",
  initialState: {
    show: false,
    isFakeHideOn: false,
    name: null,
    onCloseCallback: null,
    canClose: true,
    showClose: true,
    width: "auto",
    layer: {},
    isFetching: false,
  },
  reducers: {
    togglePopup: (state, action) => {
      const { payload = {} } = action;
      const {
        show = !!payload.name || !!payload.header || !!payload.content,
        header,
        content,
        canClose = true,
        showClose = true,
        width,
        maxWidth,
        layer,
        name,
        leftSide = false,
        closeButtonTop,
        fakeHide = false,
        isFakeHideOn = false,
        onCloseCallback,
        modalProperties,
      } = payload;

      if (layer) {
        const show = !!payload.name || !!payload.header || !!payload.content;
        const layers = {
          ...state.layer,
          [layer]: {
            show,
            canClose,
            showClose,
            header,
            content,
            width,
            maxWidth,
            name,
            leftSide,
            closeButtonTop,
            fakeHide,
            isFakeHideOn,
            onCloseCallback,
            modalProperties,
          },
        };
        if (!show) delete layers[layer];
        state.layer = layers;
      } else {
        state.show = show;
        state.name = name;
        state.canClose = canClose;
        state.showClose = showClose;
        state.header = header;
        state.content = content;
        state.width = width;
        state.leftSide = leftSide;
        state.maxWidth = maxWidth;
        state.layer = {};
        state.closeButtonTop = closeButtonTop;
        state.fakeHide = fakeHide;
        state.isFakeHideOn = isFakeHideOn;
        state.onCloseCallback = onCloseCallback;
        state.modalProperties = modalProperties;
      }
    },
    setModalLoading: (state, { payload }) => {
      state.isFetching = payload;
    },
  },
});

export const { togglePopup, setModalLoading } = _modalSlice.actions;
export const hidePopup = layer => {
  layer = typeof layer !== 'object' ? layer : false;
  return togglePopup(layer && { layer });
};
export const modalSelector = state => state.modal;
export const modalNameSelector = state => state.modal?.name;
