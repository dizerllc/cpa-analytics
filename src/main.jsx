import React from 'react';
import {
  StyleSheetManager,
} from 'styled-components';
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import { Provider } from "react-redux";
import store from './store/store.js';
import GlobalStyles from './styles/GlobalStyles.jsx';
import isPropValid from '@emotion/is-prop-valid';

ReactDOM.createRoot(document.getElementById('root')).render(
  <Provider store={store}>
    <StyleSheetManager
      enableVendorPrefixes
      shouldForwardProp={(propName, elementToBeRendered) => {
        return typeof elementToBeRendered === 'string' ? isPropValid(propName) : true;
      }}
    >
      <React.StrictMode>
        <GlobalStyles />
        <App />
      </React.StrictMode>
    </StyleSheetManager>
  </Provider>
)
