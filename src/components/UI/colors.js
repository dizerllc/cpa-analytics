import { rgba } from "./helpers";
import { css } from 'styled-components';

export const colors = {
  N900: '#04133B',
  N800: '#0B1C4A',
  N700: '#132454',
  N600: '#1D3161',
  N500: '#2F3D64',
  N400: '#546080',
  N300: '#6C7692',
  N200: '#7A869A',
  N100: '#A5ADBA',
  N75: '#C1C7D0',
  N50: '#D1D4DB',
  N40: '#DFE1E6',
  N30: '#EBECF0',
  N20: '#F4F5F7',
  N10: '#FAFBFC',
  N0: '#FFFFFF',

  G500: '#14AE95',
  G400: '#17C4A8',
  G300: '#14DCBC',
  G200: '#25E8C8',
  G100: '#6AEFD9',
  G75: '#AFF6EB',
  G50: '#DDFBF6',

  Y500: '#FF8B00',
  Y400: '#FF9920',
  Y300: '#FFB300',
  Y200: '#FFC400',
  Y100: '#FFDC69',
  Y75: '#FFF0B3',
  Y50: '#FFFAE6',

  R500: '#E10C00',
  R400: '#EE3C48',
  R300: '#FF6A61',
  R200: '#FF817B',
  R100: '#FFB2AE',
  R75: '#FFD0CE',
  R50: '#FFEAE6',

  F500: '#A42DA0',
  F400: '#C94CB9',
  F300: '#E47CCD',
  F200: '#FF95D6',
  F100: '#FFAFE0',
  F75: '#FBCEE9',
  F50: '#FCE7F4',

  P500: '#460DA3',
  P400: '#5C18CB',
  P300: '#782FEF',
  P200: '#A771FE',
  P100: '#C0A2FF',
  P75: '#D9CBFF',
  P50: '#EAE6FF',

  D500: '#112145',
  D400: '#1D3161',
  D300: '#354D87',
  D200: '#4F6EBA',
  D100: '#869BD0',
  D75: '#BDC9E5',
  D50: '#E2E7F3',

  O500: '#0D36AB',
  O400: '#1040C6',
  O300: '#045DE9',
  O200: '#2584FF',
  O100: '#72AFFF',
  O75: '#B3D4FF',
  O50: '#DEEBFF',

  B500: '#0286DD',
  B400: '#0295F6',
  B300: '#2AABFF',
  B200: '#2AB9FF',
  B100: '#77D2FF',
  B75: '#AAE3FF',
  B50: '#DDF4FF',
};

// opacity alternatives
colors.TN600 = rgba(colors.N800, 0.9);
colors.TN500 = rgba(colors.N900, 0.82);
colors.TN400 = rgba(colors.N900, 0.68);
colors.TN300 = rgba(colors.N900, 0.6);
colors.TN200 = rgba(colors.N900, 0.5);
colors.TN100 = rgba(colors.N900, 0.35);
colors.TN75 = rgba(colors.N900, 0.24);
colors.TN50 = rgba(colors.N900, 0.18);
colors.TN40 = rgba(colors.N900, 0.13);
colors.TN30 = rgba(colors.N900, 0.08);
colors.TN20 = rgba(colors.N900, 0.04);
colors.TN10 = rgba(colors.N900, 0.02);

colors.TR200 = rgba(colors.R300, 8);
colors.TR100 = rgba(colors.R300, 6);
colors.TR75 = rgba(colors.R300, 4);
colors.TR50 = rgba(colors.R300, 2);

colors.TG300 = rgba(colors.G400, 8);
colors.TG200 = rgba(colors.G400, 6);
colors.TG100 = rgba(colors.G400, 4);
colors.TG75 = rgba(colors.G400, 2);
colors.TG50 = rgba(colors.G400, 1);

// gradients
export const gradients = [
  {
    title: "Blue",
    colorStart: colors.B400,
    colorEnd: colors.B100,
    degrees: 140,
    colorStartKey: "B400",
    colorEndKey: "B100",
  },
  {
    title: "Green",
    colorStart: colors.G500,
    colorEnd: colors.G200,
    degrees: 140,
    colorStartKey: "G500",
    colorEndKey: "G200",
  },
  {
    title: "Yellow",
    colorStart: colors.Y500,
    colorEnd: colors.Y75,
    degrees: 140,
    colorStartKey: "Y500",
    colorEndKey: "Y75",
  },
  {
    title: "Red",
    colorStart: colors.R400,
    colorEnd: colors.R100,
    degrees: 140,
    colorStartKey: "R400",
    colorEndKey: "R100",
  },
  {
    title: "Flavour",
    colorStart: colors.F500,
    colorEnd: colors.F200,
    degrees: 140,
    colorStartKey: "F500",
    colorEndKey: "F200",
  },
  {
    title: "Purple",
    colorStart: colors.P400,
    colorEnd: colors.P200,
    degrees: 140,
    colorStartKey: "P400",
    colorEndKey: "P200",
  },
  {
    title: "Ocean",
    colorStart: colors.O400,
    colorEnd: colors.B300,
    degrees: 140,
    colorStartKey: "0400",
    colorEndKey: "B300",
  },
];
colors.gradient = {};
gradients.forEach(({ title, colorStart, colorEnd, degrees }) => {
  //prettier-ignore
  colors.gradient[title] = css`background: linear-gradient(${degrees}deg, ${colorStart} 0%, ${colorEnd} 100%);`;
  colors["gradient" + title] = css`
        background: linear-gradient(${degrees}deg, ${colorStart} 0%, ${colorEnd} 100%);
    `;
});

// report gtoupd colors
colors.t_editorial = colors.P400;
colors.t_home = colors.G400;

// referals colors
colors.t_main_page = "#e78a7a";
colors.t_other_pages = "#707dbf";
colors.t_other_page = "#707dbf";
colors.t_referral = "#70b5e4";
colors.t_search = colors.F300;
colors.t_social = "#367dff";
colors.t_news = "#f9c528";
colors.t_direct = "#a8c97e";

// summary metrics and chartlines colors
colors.pageviews = colors.B400;
colors.sessions = colors.F300;
colors.uniques = colors.P300;

// drill chart colors
colors.chart = {
  main_page: {
    text: colors.t_main_page,
    line: colors.t_main_page,
    fill: colors.t_main_page,
  },
  other_pages: {
    text: colors.t_other_pages,
    line: colors.t_other_pages,
    fill: colors.t_other_pages,
  },
  direct: {
    text: colors.t_direct,
    line: colors.t_direct,
    fill: colors.t_direct,
  },
  news: {
    text: colors.t_news,
    line: colors.t_news,
    fill: colors.t_news,
  },
  referral: {
    text: colors.t_referral,
    line: colors.t_referral,
    fill: colors.t_referral,
  },
  search: {
    text: colors.t_search,
    line: colors.t_search,
    fill: colors.t_search,
  },
  social: {
    text: colors.t_social,
    line: colors.t_social,
    fill: colors.t_social,
  },
};

// theme colors
colors.listHover = rgba(colors.N40, 0.25);
colors.background = colors.N900;
colors.block = colors.N800;
colors.main = colors.B300;
colors.mainHover = colors.B400;
colors.mainActive = colors.O200;
colors.light = colors.B50;
colors.lightHover = colors.B75;
colors.textColor = colors.N900;
colors.bad = colors.R300;
colors.badHover = colors.R400;
colors.good = colors.G400;
colors.disabled = colors.N75;