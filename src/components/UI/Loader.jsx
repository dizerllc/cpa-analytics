import styled, { keyframes } from 'styled-components';
import { colors } from './colors';

const rotator = keyframes`
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
`;

const dash = keyframes`
    0% {
        stroke-dasharray: 1, 60;
        stroke-dashoffset: 0;
    }
    50% {
        stroke-dasharray: 45, 60;
        stroke-dashoffset: -17;
    }
    100% {
        stroke-dasharray: 45, 60;
        stroke-dashoffset: -60;
    }
`;

const StyledTinyLoader = styled.svg`
    animation: ${rotator} 1.4s linear infinite;
    circle {
        transform-origin: center;
        animation: ${dash} 1.4s ease-in-out infinite;
    }
`;

export const TinyLoader = props => (
  <StyledTinyLoader
    width={(props.diameter || 24) + "px"}
    height={(props.diameter || 24) + "px"}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    preserveAspectRatio="none"
  >
    <circle
      fill="none"
      stroke={props.color}
      strokeWidth="3"
      strokeLinecap="round"
      cx={12}
      cy={12}
      r={10}
    />
  </StyledTinyLoader>
);


TinyLoader.defaultProps = {
  diameter: 24,
  color: colors.main,
};

export default TinyLoader;