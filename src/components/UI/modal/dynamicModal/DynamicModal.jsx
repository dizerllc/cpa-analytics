import { Suspense } from 'react';
import styled from 'styled-components';

import { TinyLoader } from '@/components/UI/Loader';

// eslint-disable-next-line no-unused-vars
export const DynamicModal = ({ name, modalProperties = {} }) => {
  const renderComponent = () => {
    switch (true) {
      default:
        return <Loading />;
    }
  };
  return <Suspense fallback={<Loading />}>{renderComponent()}</Suspense>;
};

const Loading = () => (
  <Wrapper>
    <TinyLoader diameter={28} />
  </Wrapper>
);

export default DynamicModal;

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;


