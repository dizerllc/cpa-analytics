import styled from "styled-components";
import { colors } from "../colors";
import { rgba } from "@/features/helpers";

export const SHOWN = "modalShown";

export const ModalFader = styled.div`
    display: flex;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 999;
    align-items: ${({ leftSide }) => (leftSide ? "initial" : "center")};
    justify-content: ${({ leftSide }) => (leftSide ? "flex-start" : "center")};
    background: ${rgba(colors.N900, 0.7)};
    display: none;
    padding: ${({ leftSide }) => (leftSide ? "none" : "5px 20px")};
    &.${SHOWN} {
        display: flex;
    }
    &.hasLayers {
        background: transparent;
    }
`;

export const ModalWrapper = styled('div')`
    border-radius: ${p => (p.leftSide ? 0 : "5px")};
    background: #fff;
    overflow: hidden;
    position: relative;
    color: ${colors.textColor};
    max-height: ${({ leftSide }) => (leftSide ? "100vh" : "calc(100vh - 20px)")};
    overflow-y: auto;
    transition: all 0.3s;
    /* .profileSettings_modal {
        height: 100%;
    }
    .addCampaign_modal {
        max-height: inherit;
    }
    &.subscription_holder {
        background-color: unset;
    } */
`;

export const Header = styled('div')`
  display: flex;
  flex-wrap: nowrap;
  justify-content: space-between;
  align-items: center;
  border-bottom: 1px solid ${colors.N40};
  padding: 17px 25px;
  /* &.leadfinder_export_header {
    font-weight: 700;
    padding: 25px;
  }
  &.attach_seats_to_li_accounts_header {
    font-weight: 700;
    padding: 22px 25px;
  } */
`;

export const DefaultContent = styled('div')`
    min-width: 300px;
    padding: 20px;
`;
