import styled from 'styled-components';
import { useSelector, useDispatch } from 'react-redux';
import { ModalFader, ModalWrapper, Header, DefaultContent, SHOWN } from './styled';
import { modalSelector, togglePopup } from '@/store/slices/Modal/modalSlice';
import { button } from '@/theme';
import DynamicModal from './dynamicModal/DynamicModal';
import Button from '../components/atoms/button';
import { isEmpty } from '../helpers';
import TinyLoader from '../Loader';

const Modal = () => {
  const dispatch = useDispatch();
  const modalState = useSelector(modalSelector);
  const { show, canClose, showClose, header, content, width, maxWidth, layer, leftSide, closeButtonTop, fakeHide, isFakeHideOn, isFetching, onCloseCallback, name, modalProperties } =
    modalState;

  const layers = Object.keys(layer);
  const hasLayers = !isEmpty(layer);

  const hide = layer => {
    if (onCloseCallback) {
      onCloseCallback();
    }
    if (fakeHide) {
      dispatch(togglePopup({ ...modalState, isFakeHideOn: true, layer: undefined }));
      return;
    }
    dispatch(togglePopup(layer && { layer }));
  };
  const showAfterFakeHide = fileFormat => {
    dispatch(
      togglePopup({
        ...modalState,
        isFakeHideOn: false,
        layer: undefined,
        fakeHide: false,
        name,
        modalProperties: { ...modalProperties, isError: true, format: fileFormat },
      })
    );
    return null;
  };

  const handleWrapperClick = e => {
    e.stopPropagation();
  };

  return (
    <>
      <ModalFader
        {...(isFakeHideOn && { css: { display: "none !important" } })}
        {...(show && { className: `${SHOWN} ${hasLayers ? "hasLayers" : ""}` })}
        {...(canClose && { onMouseDown: () => hide() })}
        {...(leftSide && { leftSide })}
      >
        <ModalWrapper
          onMouseDown={handleWrapperClick}
          width={width}
          maxWidth={maxWidth}
          className={`modalContent app-hidescroll ${isFetching ? "noscroll" : ""} ${name}_holder`}
          {...(leftSide && { leftSide })}
        >
          <Box className={`${name}_modal`}>
            {header && (
              <Header className={`tsH500 ${name}_header`} {...(showClose && { pr: "55px !important" })}>
                {header} {isFetching && <TinyLoader diameter={20} color={"var(--main)"} />}
              </Header>
            )}
            {showClose && <Close closeButtonTop={closeButtonTop} data-qa={"close_modal_button"} onClick={() => hide()} />}
            {content && <DefaultContent>{content}</DefaultContent>}
            {!name && !header && canClose && (
              <Box className={"textCenter"} p={20} pt={0}>
                <Button label={"Close"} themeObj={button} onClick={() => hide()} />
              </Box>
            )}
            <DynamicModal name={name} modalProperties={{ ...modalProperties, showModal: () => showAfterFakeHide() }} />
          </Box>
        </ModalWrapper>
      </ModalFader>
      {hasLayers &&
        layers.map((l, i) => {
          const { show, canClose, showClose, header, content, width, maxWidth, name, modalProperties } = layer[l];
          return (
            <ModalFader key={l} {...(show && { className: `${SHOWN} ${layers.length - 1 !== i ? "hasLayers" : ""}` })} {...(canClose && { onMouseDown: () => hide(l) })}>
              <ModalWrapper onMouseDown={handleWrapperClick} width={width} maxWidth={maxWidth} className={"modalContent lu-hidescroll"}>
                {header && (
                  <Header className={"tsH500"} {...(showClose && { pr: "55px !important" })}>
                    {header}
                  </Header>
                )}
                {showClose && <Close data-qa={"close_modal_button"} onClick={() => hide(l)} />}
                {content && <DefaultContent>{content}</DefaultContent>}
                {!name && !header && canClose && (
                  <Box className={"textCenter"} p={20} pt={0}>
                    <Button label={"Close"} themeObj={button} onClick={() => hide(l)} />
                  </Box>
                )}
                <DynamicModal name={name} modalProperties={modalProperties} />
              </ModalWrapper>
            </ModalFader>
          );
        })}
    </>
  );
};

const Close = styled(Button).attrs(() => ({
  icon: { name: "close", width: 16 },
  theme: "transparent",
  themeObj: button,
}))`
    position: absolute;
    top: ${p => p.closeButtonTop || 10}px;
    right: 10px;
    z-index: 3;
    &:hover {
        z-index: 4;
    }
`;

export default Modal;

const Box = styled.div``;
