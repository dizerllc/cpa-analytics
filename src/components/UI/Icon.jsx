import styled, { css } from "styled-components";
import icon, { iconColors } from "./icons";
import { checkCssNum } from "@/features/helpers";


const Svg = styled('svg').withConfig({
  shouldForwardProp: prop => prop !== "hoverColor"
})`
  display: inline-block;
  vertical-align: middle;
  width: ${props => checkCssNum(props.width)};
  & path {
    transition: fill .2s;
  }
  ${props =>
    props.hoverColor &&
    css`
          &:hover path {
              fill: ${props.hoverColor};
          }
      `}
  ${props => props.hoverColor && props.hoverColor !== props.color && "cursor: pointer"};
`;

const Icon = props => {
  const color = props.color;
  const icn = icon[props.name] || icon.invalid;

  if (props.image) {
    return (
      <img
        className={props.className}
        src={props.image}
        width={props.width}
        height={props.width}
        style={props.style}
        alt=""
      />
    );
  }

  return (
    <Svg
      {...props}
      className={props.className}
      viewBox="0 0 16 16"
      width={props.width}
      height={props.width}
      xmlns="http://www.w3.org/2000/svg"
      style={props.style}
      color={color}
      hoverColor={props.hoverColor && !iconColors[props.name] ? props.hoverColor : ""}
      data-name={props.name}
    >
      <g>
        {icn.map((icon, index) => {
          return <path
            className={props.className}
            key={props.name + index}
            fill={(iconColors[props.name] && iconColors[props.name][index]) || color}
            {...props.pathAttrs}
            d={icon}
            fillRule="evenodd"
          />
        })}
      </g>
    </Svg>
  );
};

export default Icon;

Icon.defaultProps = {
  color: "#fff",
  width: 16,
  hoverColor: false,
};
