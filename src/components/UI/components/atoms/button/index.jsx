import { ButtonWrpapper, FillProgress, theme } from "./styled";
import TinyLoader from "@/components/UI/Loader";
import Icon from "@/components/UI/Icon";

const Button = props => (
  <ButtonWrpapper
    data-theme={props.theme}
    data-size={props.size}
    data-group={props.group}
    data-icon={!!props.icon}
    data-ondarkbg={!!props.darkBg}
    {...props}
    as={props.as || (props.theme === "link" ? "a" : "button")}
  >
    {!!props.icon && <Icon {...props.icon} />}
    {!!props.label && <span {...props.spanAttrs}> {props.label} </span>}
    {!!props.children && <span {...props.spanAttrs}>{props.children}</span>}
    {!!props.progress && (
      <div className={"buttonSpinner"}>
        <TinyLoader diameter={16} color={props.progressColor || theme[props.theme]?.label} />
      </div>
    )}
    {!!props.fillProgress && <FillProgress value={props.fillProgress.value} />}
  </ButtonWrpapper>
);

export default Button;

Button.defaultProps = {
  theme: "default",
  size: "default",
  disabled: false,
  group: false,
  progress: false,
  br: 5,
  spanAttrs: false,
  fillProgress: false,
};
