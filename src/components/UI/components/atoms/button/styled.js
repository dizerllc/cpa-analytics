import styled, { css } from "styled-components";
import { ts100, ts200, ts300, ts400 } from "@/styles/typography";
import { checkNum, lighten, darken } from "../../../helpers";
import { colors } from "../../../../../theme";

const main = `var(--main, ${colors.main})`;
const mainHover = `var(--mainHover, ${colors.mainHover})`;
const mainActive = `var(--mainActive, ${colors.mainActive})`;
const disabled = `var(--disabled, ${colors.disabled})`;
const light = `var(--light, ${colors.light})`;
const lightHover = `var(--lightHover, ${colors.lightHover})`;
const lightActive = `var(--lightActive, ${colors.B100})`;
const lightActiveColor = `var(--lightActiveColor, ${colors.B400})`;

export const theme = {
  default: {
    label: colors.N0,
    background: main,
    border: "transparent",
    hover: {
      label: colors.N0,
      background: mainHover,
      border: "transparent",
    },
    active: {
      background: mainActive,
    },
    disabled: {
      background: disabled,
    },
  },
  danger: {
    label: colors.N0,
    background: colors.bad,
    border: "transparent",
    hover: {
      label: colors.N0,
      background: colors.badHover,
      border: "transparent",
    },
    active: {
      background: colors.R500,
    },
    disabled: {
      background: disabled,
    },
  },
  outlined: {
    label: main,
    background: "transparent",
    border: main,
    hover: {
      label: colors.N0,
      background: mainHover,
      border: mainHover,
    },
    active: {
      background: mainActive,
    },
    disabled: {
      label: disabled,
      border: disabled,
    },
    onDarkBg: {
      disabled: {
        border: "transparent",
        label: colors.N600,
        background: colors.N800,
      },
    },
  },
  light: {
    label: main,
    background: light,
    border: "transparent",
    hover: {
      label: main,
      background: lightHover,
      border: "transparent",
    },
    active: {
      label: lightActiveColor,
      background: lightActive,
    },
    disabled: {
      label: disabled,
      background: colors.N20,
    },
  },
  dark: {
    label: colors.N400,
    background: colors.N800,
    border: "transparent",
    hover: {
      label: colors.N50,
      background: colors.N700,
      border: "transparent",
    },
    active: {
      label: colors.N50,
      background: colors.N800,
    },
    disabled: {
      label: colors.N600,
      background: colors.N800,
    },
  },
  transparent: {
    label: colors.N100,
    background: "transparent",
    border: "transparent",
    minWidth: "auto",
    hover: {
      label: colors.N200,
      background: colors.TN20,
      border: "transparent",
    },
    active: {
      label: main,
      background: light,
    },
    disabled: {
      label: colors.disabled,
    },
    onDarkBg: {
      disabled: {
        border: "transparent",
        label: colors.N600,
        background: colors.N800,
      },
    },
  },
  link: {
    label: main,
    border: 0,
    padding: 0,
    height: "auto",
    minWidth: "auto",
    hover: {
      label: mainHover,
      border: 0,
    },
    disabled: {
      label: disabled,
    },
    active: {
      label: mainActive,
    },
    visited: {
      label: main,
    },
  },
};

const size = {
  default: {
    height: "38px",
    ts: ts300,
    fontWeight: 500,
  },
  big: {
    height: "50px",
    ts: ts400,
    padding: "0 20px",
  },
  small: {
    height: "30px",
    ts: ts100,
  },
};

export const ButtonWrpapper = styled.button`
    appearance: none;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    align-self: center;
    box-sizing: border-box;
    position: relative;

    ${props => {
    const formatValue = checkNum;

    let {
      p,
      br,
      icon,
      label,
      color,
      width,
      height,
      active,
      darkBg,
      disabled,
      minWidth,
      progress,
      textColor,
      hoverColor,
      borderColor,
      activeColor,
      theme: themeName,
      themeObj,
      useIconColor,
    } = props;

    width = formatValue(width);
    minWidth = formatValue(minWidth);
    height = formatValue(height);
    p = formatValue(p);
    br = formatValue(br);

    const getTheme = themeObj && themeObj[themeName] ? themeObj[themeName] : theme[themeName];
    const getSize = size[props.size];

    const correctMinWidth = width => {
      if (!width) return "100px";

      const w = parseInt(width);
      if (!w) return width;

      const mW = parseInt(getTheme.minWidth || "100px");
      return w < mW ? width : getTheme.minWidth || "100px";
    };

    return css`
            ${getSize.ts};
            font-weight: ${getSize.fontWeight};
            padding: ${p ?? getTheme.padding ?? getSize.padding ?? "0 15px"};
            min-width: ${minWidth ?? correctMinWidth(width)};
            width: ${width};
            height: ${height || getTheme.height || getSize.height};
            border-radius: ${br};
            color: ${textColor || getTheme.label};
            border: 1px solid ${borderColor || (!disabled && color) || getTheme.border || "transparent"};
            background: ${color || getTheme.background};
            ${progress &&
      css`
                color: transparent !important;
                & > img,
                & > svg {
                    display: none !important;
                }
            `};

            ${!useIconColor &&
      css`
                svg {
                    flex: none;
                    path {
                        fill: ${icon?.color || textColor || getTheme.label};
                    }
                }
            `};

            ${active &&
      css`
                background: ${theme.default.background};
                border-color: ${theme.default.background};
                color: ${theme.default.label};
            `};

            ${!progress &&
      css`
                &:not([disabled]) {
                    cursor: pointer;
                    &:hover,
                    &:active {
                        color: ${textColor || getTheme.hover.label};
                        border: 1px solid
                            ${borderColor || hoverColor || getTheme.hover.border || "transparent"};
                        background: ${hoverColor || getTheme.hover.background};
                        ${!useIconColor &&
        css`
                            svg path {
                                fill: ${textColor || getTheme.hover.label};
                            }
                        `}
                    }
                    ${getTheme.active &&
        css`
                        &:active {
                            ${getTheme.active.label &&
          css`
                                color: ${textColor || getTheme.active.label};
                            `};
                            border: 1px solid
                                ${activeColor || color || getTheme.active.border || "transparent"};
                            background: ${activeColor || color || getTheme.active.background};
                            ${!useIconColor &&
          css`
                                svg path {
                                    fill: ${textColor || getTheme.active.label};
                                }
                            `}
                        }
                    `}
                    ${getTheme.visited &&
        css`
                        &:visited {
                            ${getTheme.visited.label && `color: ${getTheme.visited.label}`};
                            ${getTheme.visited.border && `border: 1px solid ${getTheme.visited.border}`};
                            ${getTheme.visited.background && `background: ${getTheme.visited.background}`};
                        }
                    `}
                }
            `};

            &[disabled] {
                color: ${darkBg ? getTheme.onDarkBg.disabled.label : getTheme.disabled.label};
                background: ${darkBg ? getTheme.onDarkBg.disabled.background : getTheme.disabled.background};
                border-color: ${darkBg ? getTheme.onDarkBg.disabled.border : getTheme.disabled.border};
                ${!useIconColor &&
      css`
                    svg path {
                        fill: ${getTheme.disabled.label};
                    }
                `}
            }

            ${!label &&
      css`
                width: ${width || getSize.height};
                min-width: ${width || getSize.height};
                height: ${height || (width && !width.includes("%") && width) || getSize.height};
                vertical-align: middle;
                padding: 0;
                &:before {
                    content: "";
                    color: transparent;
                    ${ts200};
                }
            `}

            &[data-group="true"] {
                border-radius: 0;
                &:first-child {
                    border-top-left-radius: ${br};
                    border-bottom-left-radius: ${br};
                }
                &:last-child {
                    border-top-right-radius: ${br};
                    border-bottom-right-radius: ${br};
                }

                ${themeName === "dark" &&
      css`
                    &:not(:last-child) {
                        border-right-color: ${darken(getTheme.background, 0.1)};
                    }
                `}

                ${themeName === "default" &&
      css`
                    &:not(:last-child) {
                        border-right-color: ${lighten(getTheme.background, 0.1)};
                    }
                `}

                ${themeName === "light" &&
      css`
                    border-right-color: transparent;
                    &:not(:last-child) {
                        margin-right: 1px;
                    }
                `}

                ${themeName === "outlined" &&
      css`
                    margin-left: -1px;
                    &:first-child {
                        margin-left: 0;
                    }
                `}
            }

            .buttonSpinner {
                position: absolute;
                top: -1px;
                left: -1px;
                right: -1px;
                bottom: -1px;
                display: flex;
                justify-content: center;
                align-items: center;
                z-index: 2;
                background: ${color || getTheme.background} !important;
                border-radius: 5px;
            }
        `;
  }};

    &[data-icon="true"] {
        span {
            margin-left: 5px;
        }
    }

    &:hover {
        z-index: 1;
    }

    &:after {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        z-index: 1;
    }

    svg,
    img,
    span {
        display: inline-block;
        vertical-align: middle;
    }
    svg {
        position: relative;
        z-index: 1;
    }

    .fillProgress {
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        background: ${colors.B75};
    }

    ${props => props.fillProgress && "overflow:hidden"};
`;

export const FillProgress = styled.div.attrs(props => {
  return { style: { width: props.value + "%" } };
})`
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    background: ${colors.B75};
`;
