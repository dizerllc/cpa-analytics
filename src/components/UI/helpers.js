import _isEqual from 'lodash.isequal';
import _isEmpty from 'lodash.isempty';

import {
  differenceInMinutes,
  differenceInDays,
  differenceInHours,
  isSameDay,
  subDays,
  isThisYear,
  isSameYear,
  isSameMonth,
  isEqual as dateIsEqual,
  isDate,
  parse,
  format,
  addSeconds,
} from 'date-fns';
import { enUS, de, ru } from 'date-fns/locale';

const locales = {
  en: enUS,
  ru: ru,
  de: de,
};

// Internal helpers

export const checkNum = value => (+value === value ? value + "px" : value);

// external helpers
export const formatDate = (date, formatStr, lang = "en") => {
  return format(date, formatStr, {
    locale: locales[lang],
  });
}
export const formatNumber = (val, lang = "en") => {
  if (!val) return "0";
  let delimeter = lang === "ru" ? " " : ",";
  let decimals;

  // a bit complicated but it's OK
  val = parseFloat(val.toString().trim().replace(new RegExp(delimeter, "g"), "")).toString();

  if (!val) return 0;

  if (val.indexOf(".") !== -1) {
    decimals = parseFloat(parseFloat(val).toFixed(2));
    val = decimals.toString().split(".")[0];
    decimals = decimals.toString().split(".")[1];
  }

  let valReturn = "";
  let place = 0;

  for (let k = 0; k <= val.length; k++) {
    if (k % 3 === 0 && k !== 0) {
      valReturn = val.slice(val.length - k, val.length - k + 3) + delimeter + valReturn;
      place++;
    }
  }

  if (val.length - place * 3 !== 0)
    valReturn = val.slice(0, val.length - place * 3) + delimeter + valReturn;

  valReturn = valReturn.slice(0, -1);

  if (decimals) valReturn = valReturn + "." + decimals;

  return valReturn;
};
export const formatTime = sec => {
  const time = addSeconds(new Date(0, 0, 1, 0, 0, 0), +sec);
  return format(time, sec > 3600 ? "HH:mm:ss" : "mm:ss");
};

// manipulate with dates
export const humanizeDate = (frmt, from, to, noWords, t = t => t, lang, now = new Date()) => {
  let TODAY = t("today");
  let YESTERDAY = t("yesterday");

  TODAY = TODAY.charAt(0).toUpperCase() + TODAY.slice(1);
  YESTERDAY = YESTERDAY.charAt(0).toUpperCase() + YESTERDAY.slice(1);

  if (isDate(frmt)) {
    if (isDate(parse(from))) to = from;
    if (typeof from === "boolean") noWords = from;
    if (typeof to === "boolean") noWords = to;

    from = frmt;
    frmt = "D MMMM YYYY";
  }

  if (!to) to = from;

  function humanize(date, sameYear, sameMonth) {
    let f = frmt;
    if (isSameDay(date, now) && !noWords) return TODAY;
    if (isSameDay(subDays(date, 1), now) && !noWords) return YESTERDAY;
    if (sameYear) f = f.replace(/[y,]/gi, "");
    if (sameMonth) f = f.replace(/[m,]/gi, "");
    return formatDate(date, f, lang).trim();
  }

  if (dateIsEqual(from, to)) {
    return humanize(to, isThisYear(to));
  } else {
    return (
      humanize(
        from,
        !isSameDay(to, now) && !isSameDay(subDays(to, 1), now) && isSameYear(from, to),
        !isSameDay(to, now) && !isSameDay(subDays(to, 1), now) && isSameMonth(from, to)
      ) +
      " - " +
      humanize(to)
    );
  }
};
export const sortByValue = (array, key, dir) =>
  array.sort((a, b) => {
    return dir === "DESC" ? b[key] - a[key] : a[key] - b[key];
  });
export const isUpper = ch => "ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(ch) !== -1;

export const abbrNumber = number => {
  const SI_POSTFIXES = ["", "K", "M", "G", "T", "P", "E"];
  const tier = (Math.log10(Math.abs(number)) / 3) | 0;
  if (tier === 0) return number;
  const postfix = SI_POSTFIXES[tier];
  const scale = Math.pow(10, tier * 3);
  const scaled = number / scale;
  let formatted = scaled.toFixed(1) + "";
  if (/\.0$/.test(formatted)) formatted = formatted.substr(0, formatted.length - 2);
  return formatted + postfix;
};

// UI hepers
export const getScrollbarWidth = () => {
  const outer = document.createElement("div");
  const inner = document.createElement("div");

  outer.style.visibility = "hidden";
  outer.style.width = "100px";
  outer.style.msOverflowStyle = "scrollbar";
  outer.style.overflow = "scroll";
  outer.style.position = "absolute";
  outer.style.top = "-100px";

  inner.style.width = "100%";

  document.body.appendChild(outer);
  outer.appendChild(inner);

  setTimeout(() => {
    outer.parentNode.removeChild(outer);
  }, 0);

  return outer.offsetWidth - inner.offsetWidth;
};

export const isEqual = (a, b) => _isEqual(a, b);
export const isEmpty = arr => _isEmpty(arr);

export const randomBetween = (min, max) => Math.floor(Math.random() * (max - min + 1) + min);

export const decodeHtml = html => {
  const txt = document.createElement("textarea");
  txt.innerHTML = html;
  return txt.value;
};

export const getUrlParts = fullyQualifiedUrl => {
  if (!fullyQualifiedUrl) return {};

  let url = {},
    tempProtocol;
  const a = document.createElement("a");
  // if doesn't start with something like https:// it's not a url, but try to work around that
  if (fullyQualifiedUrl.indexOf("://") === -1) {
    tempProtocol = "https://";
    a.href = tempProtocol + fullyQualifiedUrl;
  } else a.href = fullyQualifiedUrl;
  const parts = a.hostname.split(".");

  if (parts.length < 2) url.validity = false;

  url.origin = tempProtocol ? "" : a.origin;
  url.domain = a.hostname;
  url.subdomain = parts.length > 2 ? parts[0] : "";
  url.domainroot = "";
  url.domainpath = "";
  url.tld = "." + parts[parts.length - 1];
  url.path = a.pathname.substring(1);
  url.query = a.search.substr(1);
  url.protocol = tempProtocol ? "https" : a.protocol.substr(0, a.protocol.length - 1);
  url.port = tempProtocol
    ? ""
    : a.port
      ? a.port
      : a.protocol === "http:"
        ? 80
        : a.protocol === "https:"
          ? 443
          : a.port;
  url.parts = parts;
  url.segments = a.pathname === "/" ? [] : a.pathname.split("/").slice(1);
  url.params = url.query === "" ? [] : url.query.split("&");
  for (let j = 0; j < url.params.length; j++) {
    const param = url.params[j];
    const keyval = param.split("=");
    url.params[j] = {
      key: keyval[0],
      val: keyval[1],
    };
  }

  // domainroot
  if (parts.length > 2) {
    url.domainroot = parts[parts.length - 2] + "." + parts[parts.length - 1];

    // check for country code top level domain
    if (parts[parts.length - 1].length === 2 && parts[parts.length - 2].length === 2)
      url.domainroot = parts[parts.length - 3] + "." + url.domainroot;
  } else {
    url.domainroot = url.domain;
  }
  // domainpath (domain+path without filenames)
  if (url.segments.length > 0) {
    const lastSegment = url.segments[url.segments.length - 1];
    const endsWithFile = lastSegment.indexOf(".") !== -1;
    if (endsWithFile) {
      const fileSegment = url.path.indexOf(lastSegment);
      const pathNoFile = url.path.substr(0, fileSegment - 1);
      url.domainpath = url.domain;
      if (pathNoFile) url.domainpath = url.domainpath + "/" + pathNoFile;
    } else url.domainpath = url.domain + "/" + url.path;
  } else url.domainpath = url.domain;
  return url;
};

export const getCurrencySign = currency => {
  currency = currency || "USD";
  const currencySymbols = {
    USD: "$",
    EUR: "€",
    RUB: "₽",
    UAH: "₴",
    CZK: "Kč",
  };
  return currencySymbols[currency.toUpperCase()];
};

export const hoursAgo = (now = new Date(), time, t = t => t) => {
  const diffDays = differenceInDays(now, time);
  const diffHours = differenceInHours(now, time);
  const diffMins = differenceInMinutes(now, time);

  const sub48 = diffHours <= 48;
  const subHour = diffHours < 1;
  const subMinute = diffMins < 1;

  if (subMinute) return t("about a minute ago");
  if (subHour) return t("{{diffMins}}min ago", { diffMins });
  if (sub48) return t("{{diffHours}}h ago", { diffHours });

  now = subDays(now, diffDays);
  const newDiffHours = differenceInHours(now, time);

  return newDiffHours > 0
    ? t("{{diffDays}}d {{newDiffHours}}h ago", { diffDays, newDiffHours })
    : t("{{diffDays}}d ago", { diffDays });
};

// Colors

export const hexToRgb = hex => {
  if (hex.charAt(0) === "#") {
    hex = hex.substr(1);
  }
  if (hex.length < 2 || hex.length > 6) {
    return false;
  }
  let values = hex.split(""),
    r,
    g,
    b;

  if (hex.length === 2) {
    r = parseInt(values[0].toString() + values[1].toString(), 16);
    g = r;
    b = r;
  } else if (hex.length === 3) {
    r = parseInt(values[0].toString() + values[0].toString(), 16);
    g = parseInt(values[1].toString() + values[1].toString(), 16);
    b = parseInt(values[2].toString() + values[2].toString(), 16);
  } else if (hex.length === 6) {
    r = parseInt(values[0].toString() + values[1].toString(), 16);
    g = parseInt(values[2].toString() + values[3].toString(), 16);
    b = parseInt(values[4].toString() + values[5].toString(), 16);
  } else {
    return false;
  }
  return { r, g, b };
};

export const rgbToHsl = ({ r: red, g: green, b: blue }) => {
  red = red / 255;
  green = green / 255;
  blue = blue / 255;

  const max = Math.max(red, green, blue);
  const min = Math.min(red, green, blue);
  const lightness = (max + min) / 2;

  if (max === min) return { h: 0, s: 0, l: lightness };

  let hue;
  const delta = max - min;
  const saturation = lightness > 0.5 ? delta / (2 - max - min) : delta / (max + min);
  switch (max) {
    case red:
      hue = (green - blue) / delta + (green < blue ? 6 : 0);
      break;
    case green:
      hue = (blue - red) / delta + 2;
      break;
    default:
      // blue case
      hue = (red - green) / delta + 4;
      break;
  }

  hue *= 60;
  return { h: hue, s: saturation, l: lightness };
};
export const hslToRgb = ({ h: hue, s: saturation, l: lightness }) => {
  const convert = color => Math.round(color * 255);

  if (saturation === 0) {
    return { r: convert(lightness), g: convert(lightness), b: convert(lightness) };
  }

  // formulae from https://en.wikipedia.org/wiki/HSL_and_HSV
  const huePrime = (((hue % 360) + 360) % 360) / 60;
  const chroma = (1 - Math.abs(2 * lightness - 1)) * saturation;
  const secondComponent = chroma * (1 - Math.abs((huePrime % 2) - 1));

  let red = 0;
  let green = 0;
  let blue = 0;

  if (huePrime >= 0 && huePrime < 1) {
    red = chroma;
    green = secondComponent;
  } else if (huePrime >= 1 && huePrime < 2) {
    red = secondComponent;
    green = chroma;
  } else if (huePrime >= 2 && huePrime < 3) {
    green = chroma;
    blue = secondComponent;
  } else if (huePrime >= 3 && huePrime < 4) {
    green = secondComponent;
    blue = chroma;
  } else if (huePrime >= 4 && huePrime < 5) {
    red = secondComponent;
    blue = chroma;
  } else if (huePrime >= 5 && huePrime < 6) {
    red = chroma;
    blue = secondComponent;
  }

  const lightnessModification = lightness - chroma / 2;
  const finalRed = red + lightnessModification;
  const finalGreen = green + lightnessModification;
  const finalBlue = blue + lightnessModification;
  return { r: convert(finalRed), g: convert(finalGreen), b: convert(finalBlue) };
};

export const rgba = (hex, opacity = 1) => {
  opacity = opacity > 1 ? (opacity / 100).toFixed(2) : opacity;
  if (typeof hex === "string" && typeof opacity === "number") {
    const color = hexToRgb(hex);
    return `rgba(${color.r},${color.g},${color.b},${opacity})`;
  }
  return hex;
};

export const lighten = (color, amount) => {
  if (typeof color === "string" && typeof amount === "number") {
    if (color === "transparent") return color;

    const hslColor = rgbToHsl(hexToRgb(color));
    const lightness = hslColor.l + parseFloat(amount);
    const rgb = hslToRgb({
      ...hslColor,
      l: lightness <= 0 ? 0 : lightness >= 1 ? 1 : lightness,
    });
    return `rgb(${rgb.r},${rgb.g},${rgb.b})`;
  }
  return color;
};

export const darken = (color, amount) => {
  if (typeof color === "string" && typeof amount === "number") {
    if (color === "transparent") return color;

    const hslColor = rgbToHsl(hexToRgb(color));
    const lightness = hslColor.l - parseFloat(amount);
    const rgb = hslToRgb({
      ...hslColor,
      l: lightness <= 0 ? 0 : lightness >= 1 ? 1 : lightness,
    });
    return `rgb(${rgb.r},${rgb.g},${rgb.b})`;
  }
  return color;
};
