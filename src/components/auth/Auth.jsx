import { useLayoutEffect } from 'react'
import styled from 'styled-components';
import { useUserAuth } from '../../features/context/UserAuthProvider';
import { APP_PATH } from '../../features/constants';
import { useNavigate } from 'react-router-dom';
import Icon from '../UI/Icon';
import { colors } from '../UI/colors';

export const Auth = () => {
  return (
    <Wrapper>
      <div className='container'>
        <BtnComp />
      </div>
    </Wrapper>
  )
}

export default Auth;

const Wrapper = styled.div`
	height: 100vh;
  background: #0b1c4a;
  display: grid;
  place-items: center;
  overflow: auto;
  padding: 15px;
  box-sizing: border-box;

  & .container {
    width: 385px;
    padding: 100px 70px;
    text-align: center;
    background-color: #fff;
    border-radius: 5px;
    position: relative;
    overflow: hidden;
    &_blurred {
      filter: blur(5px);
    }
  }
`;

const BtnComp = () => {
  const { user, googleSignIn } = useUserAuth();

  const navigate = useNavigate();

  useLayoutEffect(() => {
    if (user?.accessToken) {
      navigate(APP_PATH);
    }
  })

  return (
    <Button onClick={googleSignIn}>
      <Icon name='google' width={16} color={"#fff"} />
      Login with Google
    </Button>)
}
const Button = styled.button`
  display: flex;
  margin: 0 auto;
  padding: 8px 16px;
  align-items: center;
  gap: 5px;
  font-size: 16px;
  font-weight: 500;
  border: none;
  border-radius: 5px;
  color: white;
  background-color: ${colors.O200};
  transition: background-color 0.2s ease-in-out;
  cursor: pointer;
  &:hover {
    background-color: ${colors.O300};
  }
`;