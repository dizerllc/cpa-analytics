import { useUserAuth } from '@/features/context/UserAuthProvider';
import styled from 'styled-components';
import Icon from '../../UI/Icon';


export const UserProfileSettings = () => {
  const { user, logOut } = useUserAuth();

  return (
    <Wrapper onClick={logOut}>
      {user.photoURL ? <img src={user.photoURL} alt='user' className='user-img' /> : <Icon name="user" width={24} color={"#fff"} key={1} />}
      <div className='user-info'>{user.displayName || user.email}</div>
      <Icon name="exit" hoverColor="red" width={12} key={2} />
    </Wrapper>
  )
}

export default UserProfileSettings;

const Wrapper = styled.div`
  display: flex;
  width: 200px;
  height: 38px;
  align-items: center;
  padding: 5px 10px;
  gap: 10px;
  border-radius: 4px;
  user-select: none;
  transition: background-color 0.2s ease-in-out;
  /* &:hover {
    cursor: pointer;
    background-color:#1d3161;
  } */
  & .user-img {
    width: 24px;
    height: 24px;
    border-radius: 50%;
  }
  & .user-info {
    flex: 1;
    font-size: 12px;
    font-weight: 500;
    overflow: hidden;

    white-space: nowrap;
    text-overflow: ellipsis;
    color: #fff;
  }
`