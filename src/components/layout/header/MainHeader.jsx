import styled from 'styled-components';
import UserProfileSettings from './UserProfileSettings';

export const MainHeader = () => {
  return (
    <Wrapper>
      MainHeader
      <UserProfileSettings />
    </Wrapper>
  )
}

export default MainHeader;

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
	background-color: #132454;
  height: 70px;
  padding: 0 20px;
`;