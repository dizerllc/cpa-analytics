import styled from 'styled-components';

export const SidebarNavigation = () => {
  return (
    <Wrapper>Dashboard</Wrapper>
  )
}

export default SidebarNavigation;

const Wrapper = styled.div`
  display: flex;
  width: 200px;
  height: 100vh;
  padding: 20px 0 0 20px;
  color: #fff;
  background-color: #0F172A;
`;