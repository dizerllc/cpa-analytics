import { useLayoutEffect } from 'react'
import styled from 'styled-components';
import SidebarNavigation from './sidebar/SideBar/SidebarNavigation';
import MainHeader from './header/MainHeader';
import Main from './main/Main';
import { useUserAuth } from '@/features/context/UserAuthProvider';
import { useNavigate } from 'react-router-dom';
import { AUTH_PATH } from '@/features/constants';
import Modal from '../UI/modal/Modal';

export const Layout = ({ children }) => {
  const { user } = useUserAuth();
  const navigation = useNavigate()

  useLayoutEffect(() => {
    if (!user?.accessToken) {
      navigation(AUTH_PATH);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user]);

  if (!user?.accessToken) {
    return null
  }

  return (
    <>
      <Wrapper>
        <SidebarNavigation />
        <MainContainer>
          <MainHeader />
          {children}
          <Main />
        </MainContainer>
      </Wrapper>
      <Modal />
    </>
  )
}

export default Layout;

const Wrapper = styled.div`
	display: flex;
	width: 100vw;
	height: 100vh;
`;
const MainContainer = styled.div`
	flex: 1;
`;