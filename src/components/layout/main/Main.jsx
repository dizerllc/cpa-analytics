import styled from 'styled-components';

export const Main = () => {
  return (
    <Wrapper>Main</Wrapper>
  )
}

export default Main;

const Wrapper = styled.div`
  flex: 1;
  height: 100%;
  padding: 15px 20px;
  background-color: #0b1c4a;
  overflow: hidden;
`;