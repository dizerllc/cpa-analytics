import { colors as draftColors } from './components/UI/colors';

export const colors = {
  main: draftColors.P200,
  mainHover: draftColors.P300,
  mainActive: draftColors.P400,
  light: draftColors.P50,
  lightHover: draftColors.P75,
};

export const theme = {
  main: draftColors.P200,
  mainHover: draftColors.P300,
  mainActive: draftColors.P400,
  light: draftColors.P50,
  lightHover: draftColors.P75,
  ferrari_red: "#ED2224",
};

export const button = {
  default: {
    label: draftColors.N0,
    background: colors.main,
    border: "transparent",
    hover: {
      label: draftColors.N0,
      background: colors.mainHover,
      border: "transparent",
    },
    active: {
      background: colors.mainActive,
    },
    disabled: {
      background: draftColors.disabled,
    },
  },
  outlined: {
    label: colors.main,
    background: "transparent",
    border: colors.main,
    hover: {
      label: draftColors.N0,
      background: colors.mainHover,
      border: colors.mainHover,
    },
    active: {
      background: colors.mainActive,
    },
    disabled: {
      label: draftColors.disabled,
      border: draftColors.N50,
    },
    onDarkBg: {
      disabled: {
        border: "transparent",
        label: draftColors.N600,
        background: draftColors.N800,
      },
    },
  },
  link: {
    label: colors.main,
    border: 0,
    padding: 0,
    height: "auto",
    minWidth: "auto",
    hover: {
      label: colors.mainHover,
      border: 0,
    },
    disabled: {
      label: draftColors.disabled,
    },
    active: {
      label: colors.mainActive,
    },
    visited: {
      label: colors.main,
    },
  },
  darkRegular: {
    label: draftColors.N400,
    background: draftColors.N700,
    border: "transparent",
    hover: {
      label: draftColors.N0,
      background: draftColors.N500,
      border: "transparent",
    },
    active: {
      background: colors.mainActive,
    },
    disabled: {
      label: draftColors.N500,
    },
    onDarkBg: {
      disabled: {
        border: "transparent",
        label: draftColors.N600,
        background: draftColors.N800,
      },
    },
  },
  darkActive: {
    label: draftColors.N0,
    background: draftColors.N600,
    border: "transparent",
    hover: {
      label: draftColors.N0,
      background: draftColors.N600,
      border: "transparent",
    },
    active: {
      background: colors.mainActive,
    },
    disabled: {
      label: draftColors.disabled,
    },
    onDarkBg: {
      disabled: {
        border: "transparent",
        label: draftColors.N600,
        background: draftColors.N800,
      },
    },
  },
  transparent: {
    label: draftColors.N100,
    background: "transparent",
    border: "transparent",
    minWidth: "auto",
    hover: {
      label: draftColors.N200,
      background: "transparent",
      border: "transparent",
    },
    active: {
      label: colors.main,
      background: colors.light,
    },
    disabled: {
      label: draftColors.disabled,
    },
    onDarkBg: {
      disabled: {
        border: "transparent",
        label: draftColors.N600,
        background: draftColors.N800,
      },
    },
  },
  light: {
    label: colors.main,
    background: colors.light,
    border: "transparent",
    hover: {
      label: colors.mainHover,
      background: colors.lightHover,
      border: "transparent",
    },
    active: {
      label: colors.mainActive,
      background: draftColors.P100,
    },
    disabled: {
      label: draftColors.disabled,
      background: draftColors.N20,
    },
  },
  custom: {
    white: {
      white: {
        label: draftColors.N900,
        background: draftColors.N0,
        border: "transparent",
        hover: {
          label: draftColors.N900,
          background: draftColors.N0,
          border: "transparent",
        },
        active: {
          background: colors.mainActive,
        },
        disabled: {
          background: draftColors.disabled,
        },
      },
    },
    black: {
      black: {
        label: draftColors.N0,
        background: draftColors.N900,
        border: "transparent",
        hover: {
          label: draftColors.N0,
          background: draftColors.N900,
          border: "transparent",
        },
        active: {
          background: colors.mainActive,
        },
        disabled: {
          background: draftColors.disabled,
        },
      },
    },
    outlinedGreyBorder: {
      outlinedGreyBorder: {
        label: "#fff",
        background: colors.main,
        border: colors.main,
        hover: {
          label: "#fff",
          background: colors.main,
          border: colors.main,
        },
        active: {
          background: colors.mainActive,
        },
        disabled: {
          label: draftColors.disabled,
          border: draftColors.N50,
        },
        onDarkBg: {
          disabled: {
            border: "transparent",
            label: draftColors.N600,
            background: draftColors.N800,
          },
        },
      },
    },
    outlinedGreyBorderAndLabel: {
      outlinedGreyBorderAndLabel: {
        label: colors.main,
        background: draftColors.N20,
        border: colors.main,
        hover: {
          label: "#fff",
          background: colors.mainHover,
          border: colors.mainHover,
        },
        active: {
          background: colors.mainActive,
        },
        disabled: {
          label: draftColors.disabled,
          border: draftColors.N50,
        },
        onDarkBg: {
          disabled: {
            border: "transparent",
            label: draftColors.N600,
            background: draftColors.N800,
          },
        },
      },
    },

    N50BorderAndN300Label: {
      label: draftColors.N300,
      background: "#fff",
      border: draftColors.N50,
      hover: {
        background: draftColors.N20,
        border: draftColors.N50,
      },
      active: {
        background: colors.light,
      },
      disabled: {
        label: draftColors.disabled,
        border: draftColors.N50,
        cursor: "not-allowed",
      },
    },
    N75BorderAndN200Label: {
      label: draftColors.N200,
      background: "#fff",
      border: draftColors.N75,
      hover: {
        background: draftColors.N20,
        border: draftColors.N50,
      },
      active: {
        background: colors.light,
      },
      disabled: {
        label: draftColors.disabled,
        border: draftColors.N50,
        cursor: "not-allowed",
      },
    },
  },
};

export const textField = {
  default: {
    color: draftColors.N200,
    background: "transparent",
    border: draftColors.N75,
    placeholder: draftColors.N200,
    iconColor: draftColors.N75,
    hover: {
      color: draftColors.N200,
      background: "transparent",
      border: colors.main,
      placeholder: draftColors.N200,
      iconColor: draftColors.N200,
    },
  },
  transparent: {
    color: draftColors.N100,
    background: "transparent",
    border: "transparent",
    placeholder: draftColors.N200,
    iconColor: draftColors.N75,
    hover: {
      color: draftColors.N200,
      background: colors.light,
      border: "transparent",
      placeholder: draftColors.N200,
      iconColor: draftColors.N200,
    },
  },
  fullTransparent: {
    color: draftColors.N100,
    background: "transparent",
    border: "transparent",
    placeholder: draftColors.N200,
    iconColor: draftColors.N75,
    hover: {
      color: draftColors.N200,
      background: "transparent",
      border: "transparent",
      placeholder: draftColors.N300,
      iconColor: draftColors.N200,
    },
  },
  fullTransparentWithIcon: {
    color: draftColors.N900,
    background: "transparent",
    border: "transparent",
    placeholder: draftColors.N200,
    iconColor: draftColors.N50,
    hover: {
      color: draftColors.N200,
      background: "transparent",
      border: draftColors.P200,
      placeholder: draftColors.N300,
      iconColor: draftColors.N200,
    },
  },
};
