import './App.css'
import Layout from './components/layout/Layout';
import { UserAuthContextProvider } from './features/context/UserAuthProvider';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { APP_PATH, AUTH_PATH } from './features/constants';
import Auth from './components/auth/Auth';

const App = () => {
  return (
    <UserAuthContextProvider>
      <Router>
        <Routes>
          <Route path={APP_PATH} element={<Layout />} />
          <Route path={AUTH_PATH} element={<Auth />} />
        </Routes>
      </Router>
    </UserAuthContextProvider>
  )
};

export default App
