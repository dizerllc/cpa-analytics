import { createGlobalStyle } from "styled-components";
import normalize from "./normalize";
import * as typography from "./typography";
import { colors } from "../components/UI/colors";

const collectTypography = typo => `
    .${typo[0]} {
        ${typeof typo[1] === "string" ? typo[1] : typo[1].join("")}
    }
`;

const typographyCollected = () => {
  let css = "";
  for (let s in typography) css += collectTypography([s, typography[s]]);
  return css;
};

const collectGradients = Object.keys(colors.gradient)
  .map(
    gradient =>
      `.gradient${gradient} {
    ${colors.gradient[gradient].join("")}
}
`
  )
  .join(``);

export const GlobalStyles = createGlobalStyle`
    ${normalize}
    ${typographyCollected()}
    ${collectGradients}
    * {
        outline: none;
    }
    :root {
        --listHover: ${colors.listHover};
        --background: ${colors.background};
        --block: ${colors.block};
        --main: ${colors.main};
        --mainHover: ${colors.mainHover};
        --mainActive: ${colors.mainActive};
        --light: ${colors.light};
        --lightHover: ${colors.lightHover};
        --textColor: ${colors.textColor};
        --bad: ${colors.bad};
        --badHover: ${colors.badHover};
        --good: ${colors.good};
        --disabled: ${colors.disabled};
    }

    body {
        font-family: ${typography.BaseFont};
        color: ${colors.N0};
        letter-spacing: 0;
        ${typography.ts300};
        background: ${colors.N900};
    }

    a {
        text-decoration: none;
        color: var(--main);
        &:visited {
            color: var(--main);
        }
        &:hover {
            color: var(--mainHover);
        }
        &:active {
            color: var(--mainActive);
        }
    }

    .ui-chart svg {
        border-radius: 5px;
    }
    // ReChart tooltip global styles
    .recharts-tooltip-wrapper {
        z-index: 20;
    }
    .recharts-reference-dot-dot,
    .recharts-reference-line-line{
        transition: all 1.5s ease;
    }
    .first-letter {
        &:first-letter {
            text-transform: uppercase;
        }
    }
    .capitalize {
        text-transform: capitalize;
    }
    .text-center {
        text-align: center;
    }
    .text-right {
        text-align: right;
    }
    .lu-hidescroll {
        overflow-y: auto;
        -ms-overflow-style: none;  // IE 10+
        scrollbar-width: none;     // Firefox
        &::-webkit-scrollbar {
            display: none;         // Safari and Chrome
        }
    }
`;

export default GlobalStyles;
