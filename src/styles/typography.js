import { css } from "styled-components";

export const BaseFont = "Helvetica Neue, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Fira Sans, Droid Sans, sans-serif";

export const fsH900 = css`font-size: 40px;`;

export const fsH800 = css`font-size: 36px;`;

export const fsH700 = css`font-size: 30px;`;

export const fsH600 = css`font-size: 24px;`;

export const fsH500 = css`font-size: 20px;`;

export const fs400 = css`font-size: 16px;`;

export const fs300 = css`font-size: 14px;`;

export const fs200 = css`font-size: 12px; text-transform: uppercase;`;

export const fs100 = css`font-size: 12px;`;

export const fs75 = css`font-size: 10px;`;

export const fs50 = css`font-size: 8px;`;

export const tsH900 = css`
    ${fsH900};
    font-weight: bold;
    line-height: 44px;
    &.b {
        font-weight: bold;
    }
    &.mb {
        margin-bottom: 56px;
    }
`;

export const tsH800 = css`
    ${fsH800};
    font-weight: bold;
    line-height: 40px;
    &.b {
        font-weight: bold;
    }
    &.mb {
        margin-bottom: 52px;
    }
`;

export const tsH700 = css`
    ${fsH700};
    font-weight: bold;
    line-height: 32px;
    &.b {
        font-weight: bold;
    }
    &.mb {
        margin-bottom: 40px;
    }
`;

export const tsH600 = css`
    ${fsH600};
    font-weight: 500;
    line-height: 28px;
    &.b {
        font-weight: bold;
    }
    &.mb {
        margin-bottom: 40px;
    }
    &.mt {
        margin-top: 40px;
    }
`;

export const tsH500 = css`
    ${fsH500};
    font-weight: 500;
    line-height: 24px;
    &.b {
        font-weight: bold;
    }
    &.mb {
        margin-bottom: 28px;
    }
    &.mt {
        margin-top: 36px;
    }
`;

export const ts400 = css`
    ${fs400};
    font-weight: 500;
    line-height: 20px;
    &.b {
        font-weight: bold;
    }
    &.mb {
        margin-bottom: 24px;
    }
    &.mt {
        margin-top: 32px;
    }
    &.lh {
        line-height: 24px;
    }
`;

export const ts300 = css`
    ${fs300};
    line-height: 16px;
    &.b {
        font-weight: bold;
    }
    &.mt {
        margin-top: 16px;
    }
    &.mt2 {
        margin-top: 24px;
    }
    &.lh {
        line-height: 20px;
    }
`;

export const ts200 = css`
    ${fs200};
    font-weight: 500;
    line-height: 16px;
    &.mt {
        margin-top: 20px;
    }
`;

export const ts100 = css`
    ${fs100};
    font-weight: 500;
    line-height: 16px;
    &.b {
        font-weight: bold;
    }
    &.mt {
        margin-top: 16px;
    }
`;

export const ts75 = css`
    ${fs75};
    font-weight: 500;
    line-height: 14px;
    &.b {
        font-weight: bold;
    }
`;

export const ts50 = css`${fs50};
    font-weight: bold;
    line-height: 10px;
`;

export const fade80 = css`opacity: 0.8;`;

export const fade50 = css`opacity: 0.5;`;

export const fade30 = css`opacity: 0.3;`;

export const textLeft = css`text-align: left;`;

export const textCenter = css`text-align: center;`;

export const textRight = css`text-align: right;`;

export const firstLetter = css`&:first-letter {
        text-transform: uppercase;
    }`;

export const capitalize = css`text-transform: capitalize;`;
