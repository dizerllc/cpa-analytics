import { addYears } from "date-fns";
import { APP, IS_LOCAL, IS_PROD } from "./constants";

// Colors
export const checkCssNum = value => (+value === value ? value + "px" : value);
export const hexToRgb = hex => {
  if (hex.charAt(0) === "#") {
    hex = hex.substr(1);
  }
  if (hex.length < 2 || hex.length > 6) {
    return false;
  }
  let values = hex.split(""),
    r,
    g,
    b;

  if (hex.length === 2) {
    r = parseInt(values[0].toString() + values[1].toString(), 16);
    g = r;
    b = r;
  } else if (hex.length === 3) {
    r = parseInt(values[0].toString() + values[0].toString(), 16);
    g = parseInt(values[1].toString() + values[1].toString(), 16);
    b = parseInt(values[2].toString() + values[2].toString(), 16);
  } else if (hex.length === 6) {
    r = parseInt(values[0].toString() + values[1].toString(), 16);
    g = parseInt(values[2].toString() + values[3].toString(), 16);
    b = parseInt(values[4].toString() + values[5].toString(), 16);
  } else {
    return false;
  }
  return { r, g, b };
};
export const rgbToHsl = ({ r: red, g: green, b: blue }) => {
  red = red / 255;
  green = green / 255;
  blue = blue / 255;

  const max = Math.max(red, green, blue);
  const min = Math.min(red, green, blue);
  const lightness = (max + min) / 2;

  if (max === min) return { h: 0, s: 0, l: lightness };

  let hue;
  const delta = max - min;
  const saturation = lightness > 0.5 ? delta / (2 - max - min) : delta / (max + min);
  switch (max) {
    case red:
      hue = (green - blue) / delta + (green < blue ? 6 : 0);
      break;
    case green:
      hue = (blue - red) / delta + 2;
      break;
    default:
      // blue case
      hue = (red - green) / delta + 4;
      break;
  }

  hue *= 60;
  return { h: hue, s: saturation, l: lightness };
};
export const hslToRgb = ({ h: hue, s: saturation, l: lightness }) => {
  const convert = color => Math.round(color * 255);

  if (saturation === 0) {
    return { r: convert(lightness), g: convert(lightness), b: convert(lightness) };
  }

  // formulae from https://en.wikipedia.org/wiki/HSL_and_HSV
  const huePrime = (((hue % 360) + 360) % 360) / 60;
  const chroma = (1 - Math.abs(2 * lightness - 1)) * saturation;
  const secondComponent = chroma * (1 - Math.abs((huePrime % 2) - 1));

  let red = 0;
  let green = 0;
  let blue = 0;

  if (huePrime >= 0 && huePrime < 1) {
    red = chroma;
    green = secondComponent;
  } else if (huePrime >= 1 && huePrime < 2) {
    red = secondComponent;
    green = chroma;
  } else if (huePrime >= 2 && huePrime < 3) {
    green = chroma;
    blue = secondComponent;
  } else if (huePrime >= 3 && huePrime < 4) {
    green = secondComponent;
    blue = chroma;
  } else if (huePrime >= 4 && huePrime < 5) {
    red = secondComponent;
    blue = chroma;
  } else if (huePrime >= 5 && huePrime < 6) {
    red = chroma;
    blue = secondComponent;
  }

  const lightnessModification = lightness - chroma / 2;
  const finalRed = red + lightnessModification;
  const finalGreen = green + lightnessModification;
  const finalBlue = blue + lightnessModification;
  return { r: convert(finalRed), g: convert(finalGreen), b: convert(finalBlue) };
};
export const rgba = (hex, opacity = 1) => {
  opacity = opacity > 1 ? (opacity / 100).toFixed(2) : opacity;
  if (typeof hex === "string" && typeof opacity === "number") {
    const color = hexToRgb(hex);
    return `rgba(${color.r},${color.g},${color.b},${opacity})`;
  }
  return hex;
};
export const lighten = (color, amount) => {
  if (typeof color === "string" && typeof amount === "number") {
    if (color === "transparent") return color;

    const hslColor = rgbToHsl(hexToRgb(color));
    const lightness = hslColor.l + parseFloat(amount);
    const rgb = hslToRgb({
      ...hslColor,
      l: lightness <= 0 ? 0 : lightness >= 1 ? 1 : lightness,
    });
    return `rgb(${rgb.r},${rgb.g},${rgb.b})`;
  }
  return color;
};
export const darken = (color, amount) => {
  if (typeof color === "string" && typeof amount === "number") {
    if (color === "transparent") return color;

    const hslColor = rgbToHsl(hexToRgb(color));
    const lightness = hslColor.l - parseFloat(amount);
    const rgb = hslToRgb({
      ...hslColor,
      l: lightness <= 0 ? 0 : lightness >= 1 ? 1 : lightness,
    });
    return `rgb(${rgb.r},${rgb.g},${rgb.b})`;
  }
  return color;
};

//local storage
export const localSettings = {
  data: () => (localStorage.getItem(APP) ? JSON.parse(localStorage.getItem(APP)) : {}),

  get: key => localSettings.data()[key],
  set: (key, value) => {
    localStorage.setItem(
      APP,
      JSON.stringify({
        ...localSettings.data(),
        [key]: value,
      })
    );
  },
  remove: key => {
    const settings = localSettings.data();
    delete settings[key];
    localStorage.setItem(APP, JSON.stringify(settings));
    return new Promise(resolve => setTimeout(resolve, 20));
  },
  reset: () => localStorage.removeItem(APP),
};

export function cookieGet(name) {
  return document.cookie.replace(new RegExp("(?:(?:^|.*;\\s*)" + name + "\\s*=\\s*([^;]*).*$)|^.*$"), "$1");
}
export function cookieSet(name, value = 1, expires = addYears(new Date(), 1), domain = IS_PROD ? "" : "", sameSite = "none") {
  if (IS_LOCAL) {
    domain = "localhost";
  }
  document.cookie = `${name}=${value}; expires=${expires}; domain=${domain}; path=/; SameSite=${sameSite}; Secure`;
}