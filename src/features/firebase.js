// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
	apiKey: "AIzaSyCFcTsGSP2eSzOPfQ_rg3AYj5BLkV6WOeA",
	authDomain: "cpaanalytics.firebaseapp.com",
	projectId: "cpaanalytics",
	storageBucket: "cpaanalytics.appspot.com",
	messagingSenderId: "1001182206125",
	appId: "1:1001182206125:web:fb974ab0d6f1af62dece24",
	measurementId: "G-NPVWYQLF16"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export default app;