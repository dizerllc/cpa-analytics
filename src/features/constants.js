import { Mutex } from "async-mutex";
import { cookieGet } from "./helpers";

export const FETCH_MUTEX = new Mutex();
export const APP_PATH = '/';
export const AUTH_PATH = '/auth';
export const APP = 'cpa-analytics';
export const STAGE_URI = "https://graph.facebook.com/v19.0/";
export const PROD_URI = `https://api`;
const COOKIE_URI = cookieGet("api_uri");

let URI = PROD_URI;

export const TECH_WARNING = false; // in app/src/app/App.jsx
export const TECH_WARNING_MESSAGE = "The site is experiencing technical difficulties. We are working on this issue.";
export const IS_PROD = !document.location.hostname.match(/onthe.io|localhost|10.0.0.1/);
export const IS_LOCAL = document.location.hostname.match(/localhost|10.0.0.1/);

if (!IS_PROD) {
  URI = COOKIE_URI || STAGE_URI;
}
export const BASE_URI = URI;
